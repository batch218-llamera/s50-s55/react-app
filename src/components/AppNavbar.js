import { useContext, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import UserContext from '../UserContext';

export default function AppNavbar() {

    // for every 'getter', there is a 'setter'
    // useState- initial state of form
    // State to store or save user information upon user login
    // for logout button
    // const [user, setUser] = useState(localStorage.getItem('email'));
    const {user} = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">

                        {/* 
                        'as' - property that allows the component to be treated as if they are a different component gaining access to its property
                        - what function would you like to have to your homepage
                        'to' - endpoint
                        */}
                        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

                        {/* Conditional rendering */}
                        {(user.id !== null) ?
                            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                            :
                            <>
                                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                            </>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

