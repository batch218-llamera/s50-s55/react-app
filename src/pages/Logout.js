import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

export default function Logout() { // reroutes to Login Page after clicking Logout

    // consume the UserContext object and destructure it to access the user state and unsetUser function from context provider
    const { unsetUser, setUser } = useContext(UserContext);
    
        // localStorage.clear();

    // Clear the localStorage of the user's information
    unsetUser();

    // allows us to run a piece of code
    useEffect(() => {
        setUser({id: null});
    })

    return (
        <Navigate to="/login" />
    )
};