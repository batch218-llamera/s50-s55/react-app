import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
// import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login() {

    // Allows us to consume the User Context Object and properties to use for user validation
    const {user, setUser} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    // hook returns a function that lets us navigate to components
    // const navigate = useNavigate()

    // Function to simulate redirection via form submission
    function authenticate(e) {
        // Prevents page redirection via form submission
        e.preventDefault();

        // connect backend
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            
            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            } 
            else {
                    Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })

            };
        });

        // set the email if the authenticated user in the local storage.
        // localStorage.setItem('email', email);
        // sets the global user state to have properties obtained from local storage
        // setUser({email: localStorage.getItem('email')});

        // Clear input fields after submission
        setEmail('');
        setPassword('');
        // navigate('/');

        //alert(`${email} has been verified! Welcome back!`);

    };

    // connect backend
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation accross the whole application
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    };


useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }
}, [email, password]);


return (

    (user.id !== null) ?
    <Navigate to ="/courses" />
    :   
    <Form onSubmit={(e) => authenticate(e)}>
        <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email" 
                placeholder="Enter email"
                value={email}
        		onChange={(e) => setEmail(e.target.value)} 
                required
            />
        </Form.Group>

        <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Password" 
                value={password}
        		onChange={(e) => setPassword(e.target.value)}
                required
            />
        </Form.Group>

         { isActive ? 
        <Button variant="primary" type="submit" id="submitBtn">
            Submit
        </Button>
        : 
        <Button variant="danger" type="submit" id="submitBtn" disabled>
            Submit
        </Button>
    }
    </Form>
    )
}